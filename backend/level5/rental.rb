require 'date'

class Rental

  attr_reader :commission, :actions, :extras
  COMMISSION_PERCENT = 0.3

  def initialize( input_hash, discounts_hash = {})
    # Discounts hash: amount of discount starting after x days
    # I.e.{ 2: .1, 5: .3, 11: .5 }
    raise ArgumentError if discounts_hash.class != Hash

    @input_hash = input_hash
    @discounts_hash = discounts_hash
    @prices = calculate_price
    @commission = calculate_commission
    @actions = calculate_actions
    @extras = calculate_extras
  end

  private

  def calculate_price
    @input_hash['rentals'].map do |rental|
      car = get_car( rental['car_id'] )
      price_per_day = car['price_per_day']

      cost_for_car = price_per_day * get_number_of_days( rental['start_date'], rental['end_date'] )
      cost_for_car += car['price_per_km'] * rental['distance']

      cost_for_car -= get_discount( get_number_of_days( rental['start_date'], rental['end_date'] ), cost_for_car, price_per_day )

      { id: rental['id'], price: cost_for_car.to_i }
    end
  end

  def calculate_commission
    @prices.map do |price|
      remaining_commission = price[:price] * COMMISSION_PERCENT

      price[:commission] = {}

      insurance_fee = remaining_commission * 0.5
      remaining_commission -= insurance_fee

      rental = get_rental( price[:id] )
      assistance_fee = get_number_of_days( rental['start_date'], rental['end_date'] ) * 100
      remaining_commission -= assistance_fee

      drivy_fee = remaining_commission

      price[:commission][:insurance_fee] = insurance_fee.to_i
      price[:commission][:assistance_fee] = assistance_fee.to_i
      price[:commission][:drivy_fee] = drivy_fee.to_i

      price
    end
  end

  def calculate_actions
    commission.map do |rental|
      {
        id: rental[:id],
        actions: [
          {
            who: 'driver',
            type: 'debit',
            amount: rental[:price]
          },
          {
            who: 'owner',
            type: 'credit',
            amount: rental[:price] - sum_commission( rental[:commission] )
          },
          {
            who: 'insurance',
            type: 'credit',
            amount: rental[:commission][:insurance_fee]
          },
          {
            who: 'assistance',
            type: 'credit',
            amount: rental[:commission][:assistance_fee]
          },
          {
            who: 'drivy',
            type: 'credit',
            amount: rental[:commission][:drivy_fee]
          }
        ]
      }
    end
  end

  def calculate_extras
    actions.map do |action|
      rental = get_rental( action[:id] )
      action[:options] = get_option(rental['id'])
      num_days = get_number_of_days( rental['start_date'], rental['end_date'] )

      price_of_extras = 0

      if has_option?( 'gps', rental['id'] )
        action[:actions][recipient_index( action[:actions], 'owner' )][:amount] += ( 500 * num_days )
        price_of_extras += ( 500 * num_days )
      end

      if has_option?( 'baby_seat', rental['id'] )
        action[:actions][recipient_index( action[:actions], 'owner' )][:amount] += ( 200 * num_days )
        price_of_extras += ( 200 * num_days )
      end

      if has_option?( 'additional_insurance', rental['id'] )
        action[:actions][recipient_index( action[:actions], 'drivy' )][:amount] += ( 1000 * num_days )
        price_of_extras += ( 1000 * num_days )
      end

      action[:actions][recipient_index( action[:actions], 'driver' )][:amount] += price_of_extras

      action
    end
  end

  def recipient_index( actions_list, recipient )
    actions_list.each_with_index do |action, index|
      return index if action[:who] == recipient
    end

    -1
  end

  def get_discount( number_of_days, full_price, price_per_day )
    total_discount = 0
    marginal_discount = 0

    number_of_days.times do |day|
      marginal_discount = @discounts_hash[day + 1] if !@discounts_hash[day + 1].nil?
      total_discount += price_per_day * marginal_discount
    end

    total_discount
  end

  def get_car( car_id )
    @input_hash['cars'].each do |car|
      return car if car['id'] == car_id
    end

    raise ArgumentError, 'Invalid car_id'
  end

  def get_rental( rental_id )
    @input_hash['rentals'].each do |rental|
      return rental if rental['id'] == rental_id
    end
  end

  def get_number_of_days( start_date, end_date )
    ( Date.parse( end_date ) - Date.parse( start_date ) + 1 ).to_i
  end

  def sum_commission( commission_hash )
    total_commission = 0

    commission_hash.keys.each do |key|
      total_commission += commission_hash[key]
    end

    total_commission
  end

  def get_option( rental_id )
    options = []
    @input_hash['options'].each do |option|
      options << option['type'] if option['rental_id'] == rental_id
    end

    options
  end

  def has_option?( option, rental_id )
    get_option( rental_id ).include?( option )
  end
end
