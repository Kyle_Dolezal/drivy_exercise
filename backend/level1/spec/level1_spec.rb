require_relative '../main'
require 'json'
require 'facets/hash/rekey'

describe 'main.rb' do
  let( :input ) {
    {
      "cars" => [
        { "id" => 1, "price_per_day" => 2000, "price_per_km" => 10 },
        { "id" => 2, "price_per_day" => 3000, "price_per_km" => 15 },
        { "id" => 3, "price_per_day" => 1700, "price_per_km" => 8 }
      ],
      "rentals" => [
        { "id" => 1, "car_id" => 1, "start_date" => "2017-12-8", "end_date" => "2017-12-10", "distance" => 100 },
        { "id" => 2, "car_id" => 1, "start_date" => "2017-12-14", "end_date" => "2017-12-18", "distance" => 550 },
        { "id" => 3, "car_id" => 2, "start_date" => "2017-12-8", "end_date" => "2017-12-10", "distance" => 150 }
      ]
    }
  }

  let( :car_1 ) { input['cars'].first }

  describe 'get_car' do

    context 'when an id for an existing car is passed in' do
      it 'returns the json data for that car' do
        expect( get_car( car_1['id'], input ) ).to eq( car_1 )
      end
    end

    context 'when an id for a nonexisting car is passed in' do
      it 'raises an error' do
        expect{ get_car( 777 ) }.to raise_error( ArgumentError )
      end
    end
  end

  describe 'calculate_price' do
    let( :input ) {
      {
        "cars" => [
          { "id" => 1, "price_per_day" => 2000, "price_per_km" => 10 },
          { "id" => 2, "price_per_day" => 3000, "price_per_km" => 15 },
          { "id" => 3, "price_per_day" => 1700, "price_per_km" => 8 }
        ],
        "rentals" => [
          { "id" => 1, "car_id" => 1, "start_date" => "2017-12-8", "end_date" => "2017-12-10", "distance" => 100 },
          { "id" => 2, "car_id" => 1, "start_date" => "2017-12-14", "end_date" => "2017-12-18", "distance" => 550 },
          { "id" => 3, "car_id" => 2, "start_date" => "2017-12-8", "end_date" => "2017-12-10", "distance" => 150 }
        ]
      }
    }

    let( :car_1 ) { input['cars'].first }

    it 'returns proper prices for each rental' do
      output = calculate_price( input )
      expected_output = JSON.parse( File.read( 'data/expected_output.json' ) )

      expect( output.map { |elem| elem.rekey!(&:to_s) } ).to eq( expected_output['rentals'] )
    end
  end
end
