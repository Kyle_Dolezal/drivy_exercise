require 'json'
require 'date'

def calculate_price( input_hash )
  rental_prices_array = []
  input_hash['rentals'].each do |rental|
    car = get_car( rental['car_id'], input_hash )

    cost_for_car = car['price_per_day'] * ( Date.parse( rental['end_date'] ) - Date.parse( rental['start_date'] ) + 1 )
    cost_for_car += car['price_per_km'] * rental['distance']

    rental_prices_array << { id: rental['id'], price: cost_for_car.to_i }
  end

  rental_prices_array
end

def get_car( car_id, input_hash )
  input_hash['cars'].each do |car|
    return car if car['id'] == car_id
  end

  raise ArgumentError, 'Invalid car_id'
end

file = File.read( 'data/input.json' )
input_hash = JSON.parse( file )

File.open("data/output.json","w+") do |f|
  f.write( JSON.pretty_generate( { rentals: calculate_price( input_hash ) } ) )
end
