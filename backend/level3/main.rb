require 'json'
require_relative "./rental.rb"

DISCOUNTS = { 2 => 0.1, 5 => 0.3, 11 => 0.5 }

rental = Rental.new( JSON.parse( File.read( 'data/input.json' ) ), DISCOUNTS )

File.open("data/output.json","w+") do |f|
  f.write( JSON.pretty_generate( { rentals: rental.commission } ) )
end
