require_relative '../rental.rb'
require 'json'

describe 'Rental' do
  describe 'calculate_price' do
    let( :input ) {
      {
        "cars" => [
          { "id" => 1, "price_per_day" => 2000, "price_per_km" => 10 }
        ],
        "rentals" => [
          { "id" => 1, "car_id" => 1, "start_date" => "2015-12-8", "end_date" => "2015-12-8", "distance" => 100 },
          { "id" => 2, "car_id" => 1, "start_date" => "2015-03-31", "end_date" => "2015-04-01", "distance" => 300 },
          { "id" => 3, "car_id" => 1, "start_date" => "2015-07-3", "end_date" => "2015-07-14", "distance" => 1000 }
        ]
      }
    }
    let( :discounts ) { { 2 => 0.1, 5 => 0.3, 11 => 0.5 } }
    let( :rental ) { Rental.new( input, discounts ) }

    it 'returns proper prices, including commission' do
      commission = rental.commission
      expected_output = JSON.parse( File.read( 'data/expected_output.json' ) )

      expect( commission.first[:price] ).to eq( expected_output['rentals'].first['price'] )

      expect( commission.first[:commission][:insurance_fee] ).to eq( expected_output['rentals'].first['commission']['insurance_fee'] )
      expect( commission.first[:commission][:assistance_fee] ).to eq( expected_output['rentals'].first['commission']['assistance_fee'] )
      expect( commission.first[:commission][:drivy_fee] ).to eq( expected_output['rentals'].first['commission']['drivy_fee'] )
    end
  end
end
