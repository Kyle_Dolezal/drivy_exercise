require 'date'

class Rental

  def initialize( input_hash, discounts_hash = {})
    # Discounts hash: amount of discount starting after x days
    # I.e.{ 2: .1, 5: .3, 11: .5 }
    raise ArgumentError if discounts_hash.class != Hash

    @input_hash = input_hash
    @discounts_hash = discounts_hash
  end

  def calculate_price
    rental_prices_array = []
    @input_hash['rentals'].each do |rental|
      car = get_car( rental['car_id'] )
      price_per_day = car['price_per_day']

      cost_for_car = price_per_day * get_number_of_days( rental['start_date'], rental['end_date'] )
      cost_for_car += car['price_per_km'] * rental['distance']

      cost_for_car -= get_discount( get_number_of_days( rental['start_date'], rental['end_date'] ), cost_for_car, price_per_day )

      rental_prices_array << { id: rental['id'], price: cost_for_car.to_i }
    end

    rental_prices_array
  end

  private

  def get_discount( number_of_days, full_price, price_per_day )
    total_discount = 0
    marginal_discount = 0

    number_of_days.times do |day|
      marginal_discount = @discounts_hash[day + 1] if !@discounts_hash[day + 1].nil?
      total_discount += price_per_day * marginal_discount
    end

    total_discount
  end

  def get_car( car_id )
    @input_hash['cars'].each do |car|
      return car if car['id'] == car_id
    end

    raise ArgumentError, 'Invalid car_id'
  end

  def get_number_of_days( start_date, end_date )
    ( Date.parse( end_date ) - Date.parse( start_date ) + 1 ).to_i
  end
end
